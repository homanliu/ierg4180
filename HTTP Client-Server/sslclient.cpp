/* ------------------------------------------------------------ *
 * file:        sslconnect.c                                    *
 * purpose:     Example code for building a SSL connection and  *
 *              retrieving the server certificate               *
 * author:      06/12/2012 Frank4DD                             *
 *                                                              *
 * compile:     gcc -o sslconnect sslconnect.c -lssl -lcrypto   *
 * ------------------------------------------------------------ */

/* Modified by Jack Y. B. Lee
 * Date: Aug 2016
 *
 * Modifications:
 * - Compile under Win32.
 * - Handling of domain name without protocol prefix.
 * - Added support to load Windows Certificate Store certs into openssl's store.
 * - Added hostname verification as per openssl 1.0.2.
 * - Added data transfer (HTTP GET and response).
 * - Updated to use TLS instead of SSL.
 *
 * - Link with libeay32MDd.lib, ssleay32MDd.lib, WS2_32.lib
 * - Add applink.c to project or else BIO functions will not work / crash.
 * - Must compile using multi-byte character set for Windows' Crypto API to work.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <iostream>
#include <string>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <resolv.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>

#include <getopt.h>
#include "es_timer.h"

char hostname[8192];
char filename[8192];
char dest_url[8192];
char local_output[8192];

bool https_connection = true;
bool stdout_to_file = false;
bool file_path_exist = false;

// Timer
ES_Timer *timer;
long response_time = 0;
long packet_size = 0;
long content_size = 0;

using namespace std;

void args_parser(int argc, char **argv) {
	if (argc == 1) 
		strcpy(dest_url, "www.google.com");
	else 
		strncpy(dest_url, argv[1], 8192);

	const char *short_opt = "b:c:";
	struct option long_opt[] =
	{
		{ "file",       required_argument, NULL, 'b' },
		{ "verifyhost", required_argument, NULL, 'c' },

		{ NULL,       0,                 NULL,  0 }
	};

	int retstat;

	while ((retstat = getopt_long_only(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (retstat) {
			case -1:       /* no more arguments */
			case 0:        /* long options toggles */
				break;

			// Local path to store HTTP GET output
			case 'b':
				strcpy(local_output, optarg);
				stdout_to_file = true;
				break;

			// Hostname
			case 'c':
				strcpy(hostname, optarg);
				break;
		}
	}
}

int InitTrustStore(SSL_CTX *ctx) {
	if (!SSL_CTX_set_default_verify_paths(ctx)) {
	// if (!SSL_CTX_load_verify_locations(ctx, 0, "/etc/ssl/certs")) {
		fprintf(stderr, "Unable to set default verify paths.\n");
		return -1;
    } else
	   return 0;
}

int create_socket(char url_str[], BIO *out) {
	/* ---------------------------------------------------------- *
	* create_socket() creates the socket & TCP-connect to server *
	* ---------------------------------------------------------- */
	int sockfd;
	char    portnum[6] = "443";
	char      proto[6] = "";
	char      *tmp_ptr = NULL;
	int           port;
	struct hostent *host;
	struct sockaddr_in dest_addr;

	/* ---------------------------------------------------------- *
	* Remove the final / from url_str, if there is one           *
	* ---------------------------------------------------------- */
	if (url_str[strlen(url_str) - 1] == '/')
		url_str[strlen(url_str) - 1] = '\0';

	/* ---------------------------------------------------------- *
	* the first : ends the protocol string, i.e. http            *
	* ---------------------------------------------------------- */
	if (strstr(url_str, "http:") || strstr(url_str, "https:")) {
		strncpy(proto, url_str, (strchr(url_str, ':') - url_str));
		if (strstr(url_str, "http:"))
			https_connection = false;
	}
	else
		strcpy(proto, "https");

	/* ---------------------------------------------------------- *
	* the hostname starts after the "://" part                   *
	* ---------------------------------------------------------- */
	if (strstr(url_str, "://"))
		strncpy(hostname, strstr(url_str, "://") + 3, sizeof(hostname));
	else
		strncpy(hostname, url_str, sizeof(hostname));

	/* ---------------------------------------------------------- *
	* if the hostname contains a colon :, we got a port number   *
	* ---------------------------------------------------------- */
	if (strchr(hostname, ':')) {
		tmp_ptr = strchr(hostname, ':');
		/* the last : starts the port number, if avail, i.e. 8443 */
		strncpy(portnum, tmp_ptr + 1, sizeof(portnum));
		*tmp_ptr = '\0';
	}

	if (char *file_ptr = strchr(hostname, '/')) {
		file_path_exist = true;

		strncpy(filename, file_ptr, strlen(file_ptr));
		file_ptr[0] = '\0';
	}

	port = atoi(portnum);

	if ((host = gethostbyname(hostname)) == NULL) {
		BIO_printf(out, "Error: Cannot resolve hostname %s.\n", hostname);
		abort();
	}

	/* ---------------------------------------------------------- *
	* create the basic TCP socket                                *
	* ---------------------------------------------------------- */
	sockfd = socket(AF_INET, SOCK_STREAM, 0);

	dest_addr.sin_family = AF_INET;
	dest_addr.sin_port = htons(port);
	dest_addr.sin_addr.s_addr = *(long*)(host->h_addr);

	/* ---------------------------------------------------------- *
	* Zeroing the rest of the struct                             *
	* ---------------------------------------------------------- */
	memset(&(dest_addr.sin_zero), '\0', 8);

	tmp_ptr = inet_ntoa(dest_addr.sin_addr);

	/* ---------------------------------------------------------- *
	* Try to make the host connect here                          *
	* ---------------------------------------------------------- */
	if (connect(sockfd, (struct sockaddr *) &dest_addr,
		sizeof(struct sockaddr)) == -1) {
		BIO_printf(out, "Error: Cannot connect to host %s [%s] on port %d.\n",
			hostname, tmp_ptr, port);
		exit(-1);
	}

	return sockfd;
}

int ssl_verification(SSL *ssl, int server_socket, BIO *outbio) {
	X509                *cert = NULL;
	X509_NAME       *certname = NULL;
	char *ptr = NULL;

	/* ---------------------------------------------------------- *
	* Attach the SSL session to the socket descriptor            *
	* ---------------------------------------------------------- */
	SSL_set_fd(ssl, server_socket);

    /* ---------------------------------------------------------- *
	* Server Name Indication (SNI) - Required by Google servers  *
	* ---------------------------------------------------------- */
    SSL_set_tlsext_host_name(ssl, hostname);

	/* ---------------------------------------------------------- *
	* Try to SSL-connect here, returns 1 for success             *
	* ---------------------------------------------------------- */
	if (SSL_connect(ssl) != 1)
		BIO_printf(outbio, "Error: Could not build a SSL session to: %s.\n", dest_url);
	else
		BIO_printf(outbio, "Successfully enabled SSL/TLS session to: %s.\n", dest_url);

	/* ---------------------------------------------------------- *
	* Get the remote certificate into the X509 structure         *
	* ---------------------------------------------------------- */
	cert = SSL_get_peer_certificate(ssl);
	if (cert == NULL) {
		BIO_printf(outbio, "Error: Could not get a certificate from: %s.\n", dest_url);
		exit(1);
	}
	else
		BIO_printf(outbio, "Retrieved the server's certificate from: %s.\n", dest_url);

	/* ---------------------------------------------------------- *
	* extract various certificate information                    *
	* -----------------------------------------------------------*/
	certname = X509_NAME_new();
	certname = X509_get_subject_name(cert);

	/* ---------------------------------------------------------- *
	* display the cert subject here                              *
	* -----------------------------------------------------------*/
	BIO_printf(outbio, "Displaying the certificate subject data:\n");
	X509_NAME_print_ex(outbio, certname, 0, 0);
	BIO_printf(outbio, "\n");

	/* ---------------------------------------------------------- *
	* Validate the remote certificate is from a trusted root     *
	* ---------------------------------------------------------- */
	int ret = SSL_get_verify_result(ssl);
	if (ret != X509_V_OK)
		BIO_printf(outbio, "Warning: Validation failed for certificate from: %s.\n", dest_url);
	else
		BIO_printf(outbio, "Successfully validated the server's certificate from: %s.\n", dest_url);

	/* ---------------------------------------------------------- *
	* Perform hostname validation                                 *
	* ---------------------------------------------------------- */
	ret = X509_check_host(cert, hostname, strlen(hostname), 0, &ptr);
	if (ret == 1) {
		BIO_printf(outbio, "Successfully validated the server's hostname matched to: %s.\n", ptr);
		OPENSSL_free(ptr); ptr = NULL;
	}
	else if (ret == 0)
		BIO_printf(outbio, "Server's hostname validation failed: %s.\n", hostname);
	else
		BIO_printf(outbio, "Hostname validation internal error: %s.\n", hostname);
	
	X509_free(cert);
	return ret;
}


int main(int argc, char **argv) {
	BIO *outbio = NULL;
	const SSL_METHOD *method;
	SSL_CTX *ctx;
	SSL *ssl;
	int server = 0;
	FILE *fp;
	
	args_parser(argc, argv);
	OpenSSL_add_all_algorithms();
	ERR_load_BIO_strings();
	ERR_load_crypto_strings();
	SSL_load_error_strings();

	outbio = BIO_new_fp(stdout, BIO_NOCLOSE);

	if (SSL_library_init() < 0)
		BIO_printf(outbio, "Could not initialize the OpenSSL library !\n");

	server = create_socket(dest_url, outbio);
	if (server != 0)
		BIO_printf(outbio, "Successfully made the TCP connection to: %s.\n", dest_url);

	char request[8192];
	sprintf(request,
		   "GET %s HTTP/1.1\r\n"
		   "Host: %s\r\n"
		   "Connection: close\r\n\r\n", (file_path_exist == true) ? filename : "/", hostname);

	if (https_connection == true) {
		method = TLS_method();

		if ((ctx = SSL_CTX_new(method)) == NULL)
			BIO_printf(outbio, "Unable to create a new SSL context structure.\n");

		InitTrustStore(ctx);
		ssl = SSL_new(ctx);

		ssl_verification(ssl, server, outbio);

		// Send an HTTPS GET request
		SSL_write(ssl, request, strlen(request));
	}
	else {
		send(server, request, strlen(request), 0);
	}

	char header_buffer[1536];
	timer = new ES_Timer();
	timer->Start();

	if (stdout_to_file == true)
		fp = fopen(local_output, "wb+");

	if (stdout_to_file == false)
		BIO_printf(outbio, "------------------ RESPONSE RECEIVED ---------------------\n");
	
	int len = 0;
	do
	{
		char buff[1536];
		if (https_connection == true)
			len = SSL_read(ssl, buff, sizeof(buff));
		else
			len = recv(server, buff, sizeof(buff), 0);

		if (len > 0) {
			if (stdout_to_file == true)
				fwrite(buff, 1, len, fp);
			else
				BIO_write(outbio, buff, len);
			
			if (packet_size == 0)
				memcpy(header_buffer, buff, len);
			
			packet_size += len;
		}
	} while (len > 0);
	response_time = timer->Elapsed();
	response_time = (response_time == 0) ? 1 : response_time;
	delete timer;

	if (stdout_to_file == false)
		BIO_printf(outbio, "\n----------------------------------------------------------\n");
	// =========================

	if (stdout_to_file == true)
		fclose(fp);


	// Retrieve content length in HTTP header
	string header(header_buffer);
	size_t pos = header.find("\r\n\r\n");
	content_size = packet_size - pos;
	// ======================================

	close(server);
	BIO_printf(outbio, "\nFinished SSL/TLS connection with server: %s.\n", dest_url);

	printf("Response Time [%ldms] Total [%ldB, %.2fKBps] File [%ldB, %.2fKBps]\n", response_time
																			     , packet_size, packet_size / (double) response_time
																			     , content_size, content_size / (double) response_time);

	return 0;
}