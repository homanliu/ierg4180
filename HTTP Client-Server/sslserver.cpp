/* ------------------------------------------------------------------ *
 * file:        sslserver.c                                           *
 * purpose:     Example code for building a SSL server with a         *
 *              local server certificate                              *
 * author:      https://wiki.openssl.org/index.php/Simple_TLS_Server  *
 *              Modified by Jack Y. B. Lee                            *
 *                                                                    *
 * compile:     gcc -o sslconnect sslconnect.c -lssl -lcrypto         *
 * ------------------------------------------------------------------ */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <resolv.h>

#include <openssl/bio.h>
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>
#include <openssl/x509v3.h>
#include <openssl/x509_vfy.h>

#include <getopt.h>
#include "tinythread.h"
#include "es_timer.h"
#include "pipe.h"

using namespace std;
using namespace tthread;

// Function rename
#define Sleep(x) usleep(x * 1000)

// Configuration
char *hostname = NULL;
int http_port = 4180, https_port = 4181, stat_update = 0;
int pool_size = 8, min_pool_size = 8, max_pool_size = 1024;
bool thread_pool = true;
// =============

// SSL Variables
SSL_CTX *ctx;
// =============

// Client threads
bool keep_running = true;
tthread::thread **connection_thread;
tthread::mutex *thread_lock;
// ==============

// Thread pool variables
pipe_t* p;
pipe_producer_t* producers[2];
pipe_consumer_t** consumers;
ES_Timer* counter = new ES_Timer();
// ====================

// Display Variables
ES_Timer* timer = new ES_Timer();
int http_client = 0, https_client = 0;
int next_client = 0;
// =================

// Thread Structure Variable
struct operating_parameters {
	int socket_descriptor = 0;

	// HTTPS = true, HTTP = false
	bool secure_connection = true;
};
// =========================

void statistics_display(void *input) {
	long next_display_time = stat_update;

	if (stat_update == 0)
		return;

	while (keep_running == true) {
		long current_time = timer->Elapsed();
		if (current_time >= next_display_time) {
         printf("Elapsed [%lds] HTTP Clients[%d] HTTPS Clients[%d]\n", current_time / 1000, http_client, https_client);
			next_display_time += stat_update;
		}
		else {
			Sleep((next_display_time - current_time));
		}
	}
	return;
}

void args_parser(int argc, char **argv) {
	const char *short_opt = "b:c:d:e:m:n:";
	struct option long_opt[] =
	{
		{ "stat",      required_argument, NULL, 'b' },

		{ "lhost",     required_argument, NULL, 'c' },
		{ "lhttpport", required_argument, NULL, 'd' },
      { "lhttpsport",required_argument, NULL, 'e' },

		{ "server",    required_argument, NULL, 'm' },
		{ "poolsize",  required_argument, NULL, 'n'},

		{ NULL,       0,                 NULL,  0 }
	};

	int retstat;

	while ((retstat = getopt_long_only(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (retstat) {
		case -1:       /* no more arguments */
		case 0:        /* long options toggles */
			break;

			// Update of statistics display
		case 'b':
			stat_update = atoi(optarg);
			break;

			// Hostname
		case 'c':
			hostname = (char *)calloc(strlen(optarg) + 1, sizeof(char));
			strcpy(hostname, optarg);
			break;

			// HTTP port number
		case 'd':
			http_port = atoi(optarg);
			break;

      	// HTTP port number
		case 'e':
			https_port = atoi(optarg);
			break;

		case 'm':
			if (strcmp(optarg, "thread") == 0)
				thread_pool = false;
			break;
		
		case 'n':
			pool_size = atoi(optarg);
			min_pool_size = pool_size;
			break;
		};
	}
}

int create_socket(int port) {
   int s;
   struct sockaddr_in addr;

   addr.sin_family = AF_INET;
   addr.sin_port = htons(port);
   addr.sin_addr.s_addr = htonl(INADDR_ANY);

   s = socket(AF_INET, SOCK_STREAM, 0);
   if (s < 0) {
      perror("Unable to create socket");
      printf("Error code = %i", errno);
      exit(EXIT_FAILURE);
   }

   int reuse_address = 1;
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse_address, sizeof(reuse_address)) == -1) {
		printf("TCP Socket setsockopt failed. Error code: %i\n", errno);
		exit(EXIT_FAILURE);
	}


   if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
      perror("Unable to bind");
      exit(EXIT_FAILURE);
   }

   if (listen(s, 1) < 0) {
      perror("Unable to listen");
      exit(EXIT_FAILURE);
   }

   return s;
}

void init_openssl() {
   SSL_load_error_strings();
   OpenSSL_add_ssl_algorithms();
}

void cleanup_openssl() {
   EVP_cleanup();
}

SSL_CTX *create_context() {
   const SSL_METHOD *method;
   SSL_CTX *ctx;

   //method = SSLv23_server_method();
   method = TLS_server_method();

   ctx = SSL_CTX_new(method);
   if (!ctx) {
      perror("Unable to create SSL context");
      ERR_print_errors_fp(stderr);
      exit(EXIT_FAILURE);
   }

   return ctx;
}

void configure_context(SSL_CTX *ctx) {
   SSL_CTX_set_ecdh_auto(ctx, 1);

   /* Set the key and cert */
   if (SSL_CTX_use_certificate_file(ctx, "cert.pem", SSL_FILETYPE_PEM) <= 0) {
      ERR_print_errors_fp(stderr);
      exit(EXIT_FAILURE);
   }

   if (SSL_CTX_use_PrivateKey_file(ctx, "key.pem", SSL_FILETYPE_PEM) <= 0) {
      ERR_print_errors_fp(stderr);
      exit(EXIT_FAILURE);
   }
}

void http_handler(void *input) {
   char reply[1024];
   sprintf(reply,
		   "HTTP/1.1 200 OK\r\n"
		   "Connection: close\r\n"
         "Content-Type: text/html\r\n"
		   "Content-Language: en\r\n\r\n"
         "<HTML>\r\n"
         "<BODY>\r\n"
         "[IERG4180] A response from HTTP Server!\r\n"
         "</BODY>\r\n"
         "</HTML>\r\n\r\n");
   
   int client_socket = *((int *) input);

   int ret = send(client_socket, reply, strlen(reply), 0);
   if (ret == -1) {
      printf("Server: send failed. Error code: %i\n", errno);
   }

   thread_lock->lock();
   http_client -= 1;
   if (thread_pool == false)
      next_client -= 1;
   thread_lock->unlock();

   close(client_socket);
}

void https_handler(void *input) {
   SSL *ssl;
   char buffer[1024];
   char reply[161];
   memset(reply, 0, 161);
   sprintf(reply,
		   "HTTP/1.1 200 OK\r\n"
		   "Connection: close\r\n"
         "Content-Type: text/html\r\n"
		   "Content-Language: en\r\n\r\n"
         "<HTML>\r\n"
         "<BODY>\r\n"
         "[IERG4180] A response from HTTPS Server with SSL!\r\n"
         "</BODY>\r\n"
         "</HTML>\r\n\r\n");

   int client_socket = *((int *) input);

   ssl = SSL_new(ctx);
   SSL_set_fd(ssl, client_socket);

   if (SSL_accept(ssl) <= 0) {
      ERR_print_errors_fp(stderr);
   }
   else {
      if (SSL_read(ssl, buffer, 1024) < 0) {
         printf("\n SSL_read failed!\n");
      }

      if (SSL_write(ssl, reply, strlen(reply)) != strlen(reply)) {
         printf("\n SSL_write failed!\n");
      }
   }

   thread_lock->lock();
   https_client -= 1;
   if (thread_pool == false)
      next_client -= 1;
   thread_lock->unlock();

   SSL_free(ssl);
   close(client_socket);
}

void connection_handler(void *input) {
   struct operating_parameters *params = (struct operating_parameters *) malloc(sizeof(struct operating_parameters));
	pipe_consumer_t *consumer = (pipe_consumer_t *) input;

	while (keep_running == true) {
		int retstat = pipe_pop(consumer, params, 1);

		if (params->secure_connection == true)
         https_handler(&params->socket_descriptor);
      else
         http_handler(&params->socket_descriptor);
	}
	
	pipe_consumer_free(consumer);
}

void http_connect(void *input) {
   int socket = create_socket(http_port);

	while (keep_running == true) {
		struct sockaddr_in addr;
      socklen_t len = sizeof(addr);
      

      int client = accept(socket, (struct sockaddr*)&addr, &len);
      if (client < 0) {
         perror("Unable to accept");
         exit(EXIT_FAILURE);
      }

      struct operating_parameters *params = (struct operating_parameters *) malloc(sizeof(struct operating_parameters));
      params->socket_descriptor = client;
      params->secure_connection = false;

      if (thread_pool == true) {
         pipe_push(producers[0], params, 1);

         thread_lock->lock();
         http_client += 1;
         thread_lock->unlock();
      }
      else {
         thread_lock->lock();
         connection_thread[next_client] = new thread(http_handler, &client);
         next_client += 1;
         http_client += 1;
         thread_lock->unlock();
      }

      free(params);
	}

   close(socket);
}

void https_connect(void *input) {
   int socket = create_socket(https_port);

	while (keep_running == true) {
		struct sockaddr_in addr;
      socklen_t len = sizeof(addr);

      int client = accept(socket, (struct sockaddr*)&addr, &len);
      if (client < 0) {
         perror("Unable to accept");
         exit(EXIT_FAILURE);
      }

      struct operating_parameters *params = (struct operating_parameters *) malloc(sizeof(struct operating_parameters));
      params->socket_descriptor = client;
      params->secure_connection = true;

      if (thread_pool == true) {
         pipe_push(producers[1], params, 1);

         thread_lock->lock();
         https_client += 1;
         thread_lock->unlock();
      }
      else {
         thread_lock->lock();
         connection_thread[next_client] = new thread(https_handler, &client);
         next_client += 1;
         https_client += 1;
         thread_lock->unlock();
      }

      free(params);
	}

   close(socket);
}

int main(int argc, char **argv) {
   // Ignore SIGPIPE Signal
   signal(SIGPIPE, SIG_IGN);
   // =====================

   // Argument Parser
   args_parser(argc, argv);
   // ===============

   // Initialize SSL
   init_openssl();
   ctx = create_context();
   configure_context(ctx);
   // ==============

   // Start Display Timer
   timer->Start();
   // ===================

   // Thread Initialization
   thread *statistics_thread = new thread(statistics_display, NULL);
   thread *http_thread = new thread(http_connect, NULL);
   thread *https_thread = new thread(https_connect, NULL);
   // =====================

   thread_lock = new tthread::mutex();
   connection_thread = (tthread::thread **) malloc(sizeof(tthread::thread *) * pool_size);

   if (thread_pool == true) {
      // Create Thread Pool
		consumers = (pipe_consumer_t **) malloc(sizeof(pipe_consumer_t *) * pool_size);

		p = pipe_new(sizeof(struct operating_parameters), 0);
		producers[0] = pipe_producer_new(p);
		producers[1] = pipe_producer_new(p);
		for (int i = 0; i < pool_size; i++)
			consumers[i] = pipe_consumer_new(p);
      // ==================

      // Create Threads
		for (int i = 0; i < pool_size; i++)
			connection_thread[i] = new thread(connection_handler, consumers[i]);
      // ==============

      pipe_free(p);
	}

   // Thread Collection
   statistics_thread->join();
   http_thread->join();
   https_thread->join();

   delete statistics_thread;
   delete http_thread;
   delete https_thread;
   // =================

   if (thread_pool == true) {
      // Collect Thread Pool Related Variables
		pipe_producer_free(producers[0]);
		pipe_producer_free(producers[1]);
		for (int i = 0; i < pool_size; i++)
			pipe_consumer_free(consumers[i]);

		delete thread_lock;
		// =====================================
   }
   
   // Free SSL Resources
   SSL_CTX_free(ctx);
   cleanup_openssl();
   // ==================
}
