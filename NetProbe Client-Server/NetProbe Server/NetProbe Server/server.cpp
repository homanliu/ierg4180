#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#ifdef _WIN32
	// Print console from thread
	#include <tchar.h>
	#include <strsafe.h>

	// Socket programming
	#include <WinSock2.h>
	#include <WS2tcpip.h>

	// Option parsing
	#include "getopt.h"

	// Need to link with Ws2_32.lib
	#pragma comment (lib, "Ws2_32.lib")

	#define get_socket_error() WSAGetLastError()
#else // Assume Linux
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <sys/ioctl.h>
	#include <sys/fcntl.h>
	#include <netinet/in.h>
	#include <netinet/tcp.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <unistd.h>
	#include <errno.h>

	#include <getopt.h>

	// Function rename
	#define get_socket_error() (errno)
	#define closesocket(s) close(s)
	#define Sleep(x) usleep(x * 1000)

	// Variable rename
	#define SOCKET int
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	#define SD_SEND SHUT_WR
	#define WSAEMSGSIZE 10040
#endif

#include "tinythread.h"
#include "es_timer.h"

using namespace std;
using namespace tthread;

#define SEND_MODE 1
#define RECV_MODE -1

// Configuration
char *hostname = NULL, protocol[4] = "UDP";
int port_number = 4180, stat_update = 0;
int sbufsize = 0, rbufsize = 0;

struct incoming_client {
	// TCP = true, UDP = false
	bool connection_type = false;
	SOCKET socket_descriptor;
	struct sockaddr_in client_addr;

	// Server receive = true, Server send = false
	bool connection_mode = false;

	int packet_size = 0;
	int packet_rate = 0;
	long packet_number = 0;

	long next_packet_number = 0;
	long next_packet_transmission_time = 0;
	long time_interval = 0;
};

struct operating_parameters {
	// TCP = true, UDP = false
	bool connection_type = false;
	// Server receive = true, Server send = false
	bool connection_mode = false;

	int packet_size = 0;
	int packet_rate = 0;
	long packet_number = 0;
};

// Socket variables
struct sockaddr_in sock_addr;
SOCKET tcp_socket, udp_socket;
fd_set socket_read_set, socket_write_set;

// Clients
struct incoming_client **connecting_client;
int next_client = 0, max_client = 10;

// Timer
ES_Timer timer = ES_Timer();

// Display thread
bool keep_running = true;
int tcp_client = 0, udp_client = 0;

void statistics_display(void *input) {
	long next_display_time = stat_update;

	if (stat_update == 0)
		keep_running = false;

	while (keep_running == true) {
		long current_time = timer.Elapsed();
		if (current_time >= next_display_time) {
			printf("Elapsed [%lds] TCP Clients [%d] UDP Clients [%d]\n", current_time / 1000, tcp_client, udp_client);
			next_display_time += stat_update;
		}
		else {
			Sleep((next_display_time - current_time));
		}
	}

	return;
}

void args_parser(int argc, char **argv) {
	const char *short_opt = "b:c:d:i:j:";
	struct option long_opt[] =
	{
		{ "stat",     required_argument, NULL, 'b' },

		{ "lhost",    required_argument, NULL, 'c' },
		{ "lport",    required_argument, NULL, 'd' },

		{ "sbufsize", required_argument, NULL, 'i' },
		{ "rbufsize", required_argument, NULL, 'j' },

		{ NULL,       0,                 NULL,  0 }
	};

	int retstat;

	while ((retstat = getopt_long_only(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (retstat) {
		case -1:       /* no more arguments */
		case 0:        /* long options toggles */
			break;

			// Update of statistics display
		case 'b':
			stat_update = atoi(optarg);
			break;

			// Hostname
		case 'c':
			hostname = (char *)calloc(strlen(optarg) + 1, sizeof(char));
			strcpy(hostname, optarg);
			break;

			// Port number
		case 'd':
			port_number = atoi(optarg);
			break;

			// Buffer size
		case 'i':
			sbufsize = atoi(optarg);
			break;

		case 'j':
			rbufsize = atoi(optarg);
			break;
		};
	}
}

void debug_args(struct incoming_client *client_info) {
	printf("Incoming Client Parameters: \n");
	printf("Client IP: %s\n", inet_ntoa(client_info->client_addr.sin_addr));
	printf("Client Port number: %d\n", ntohs(client_info->client_addr.sin_port));
	printf("Protocol: %s\n", (client_info->connection_type) ? "TCP" : "UDP");
	printf("Receive from client: %s\n", (client_info->connection_mode) ? "true" : "false");
	printf("Packet size: %d\n", client_info->packet_size);
	printf("Packet rate: %d\n", client_info->packet_rate);
	printf("Packet number: %ld\n", client_info->packet_number);
	printf("==================================\n\n");
}

void socket_cleanup() {
	// struct linger optval;
	// optval.l_onoff = 1;
	// optval.l_linger = 5;
	// setsockopt(tcp_socket, SOL_SOCKET, SO_LINGER, (char *)&optval, sizeof(optval));

	keep_running = false;

	if (tcp_socket != INVALID_SOCKET)
		closesocket(tcp_socket);

	if (udp_socket != INVALID_SOCKET)
		closesocket(udp_socket);

	#ifdef _WIN32
		WSACleanup();
	#endif
}

void socket_init() {
	int iResult;
	#ifdef _WIN32
		WSADATA wsaData;
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed: %d\n", iResult);
			exit(1);
		}
	#endif

	memset(&sock_addr, 0, sizeof(struct sockaddr_in));
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(port_number);

	// Resolve hostname to address
	if (strcmp(hostname, "IN_ADDR_ANY") == 0) {
		sock_addr.sin_addr.s_addr = INADDR_ANY;
	}
	else {
		sock_addr.sin_addr.s_addr = inet_addr(hostname);
		if (sock_addr.sin_addr.s_addr == -1) {
			struct hostent *host_result = gethostbyname(hostname);
			if (host_result != NULL) {
				// Debug IP address
				struct in_addr addr = { 0, };
				addr.s_addr = *(u_long *)host_result->h_addr_list[0];
				printf("Resolved IP Address: %s\n", inet_ntoa(addr));

				sock_addr.sin_addr.s_addr = inet_addr(inet_ntoa(addr));
			}
		}
	}
	// ============================

	// Get socket descriptor
	tcp_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (tcp_socket == INVALID_SOCKET) {
		printf("TCP Socket failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}

	udp_socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (udp_socket == INVALID_SOCKET) {
		printf("UDP Socket failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}
	// ====================

	// TCP bind and listen socket
	iResult = bind(tcp_socket, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));
	if (iResult == SOCKET_ERROR) {
		printf("Server: TCP bind failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}

	listen(tcp_socket, 128);
	if (iResult == SOCKET_ERROR) {
		printf("Server: TCP listen failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}
	// ===================

	// UDP bind socket
	iResult = bind(udp_socket, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));
	if (iResult == SOCKET_ERROR) {
		printf("Server: UDP bind failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}
	// ===============

	// Set send and receive buffer size
	if (sbufsize > 0) {
		iResult = setsockopt(tcp_socket, SOL_SOCKET, SO_SNDBUF, (char *)&sbufsize, sizeof(sbufsize));
		if (iResult < 0) {
			printf("Server: set socket sbufsize error. Error code: %i\n", get_socket_error());
			socket_cleanup();
			exit(1);
		}
	}
	if (rbufsize > 0) {
		iResult = setsockopt(tcp_socket, SOL_SOCKET, SO_RCVBUF, (char *)&rbufsize, sizeof(rbufsize));
		if (iResult < 0) {
			printf("Server: set socket rbufsize error. Error code: %i\n", get_socket_error());
			socket_cleanup();
			exit(1);
		}
	}
	// ================================
}

bool sockaddr_equal(struct sockaddr_in *input1, struct sockaddr_in *input2) {
	if (ntohs(input1->sin_port) != ntohs(input2->sin_port))
		return false;
	if (strcmp(inet_ntoa(input1->sin_addr), inet_ntoa(input2->sin_addr)) != 0)
		return false;
	
	return true;
}

SOCKET max_socket() {
	SOCKET max_sd = max(tcp_socket, udp_socket);
	for (int i = 0; i < next_client; i++)
		max_sd = max(max_sd, connecting_client[i]->socket_descriptor);
	return (max_sd + 1);
}

void concurrent_client() {
	// Clear descriptor set
	FD_ZERO(&socket_read_set);
	FD_ZERO(&socket_write_set);

	SOCKET max_sd = max_socket();

	while(true) {
		// Set all sockets into socket read set
		FD_SET(tcp_socket, &socket_read_set);
		FD_SET(udp_socket, &socket_read_set);
		FD_SET(udp_socket, &socket_write_set);
		for (int i = 0; i < next_client; i++) {
			if (connecting_client[i]->connection_type == true) {
				if (connecting_client[i]->connection_mode == true)
					FD_SET(connecting_client[i]->socket_descriptor, &socket_read_set);
				else
					FD_SET(connecting_client[i]->socket_descriptor, &socket_write_set);
			}
		}
		max_sd = max_socket();

		// Check any available socket descriptor
		int retstat = select(max_sd, &socket_read_set, &socket_write_set, NULL, NULL);
		if (retstat < 0) {
			printf("Select socket failed. Error code: %i\n", get_socket_error());
			socket_cleanup();
			exit(1);
		}

		// TCP socket is available to read
		if (FD_ISSET(tcp_socket, &socket_read_set)) {
			struct incoming_client *new_client = (struct incoming_client *) malloc(sizeof(struct incoming_client));
			memset(&new_client->client_addr, 0, sizeof(struct sockaddr_in));
			socklen_t sockaddr_len = sizeof(new_client->client_addr);

			new_client->socket_descriptor = accept(tcp_socket, (struct sockaddr *) &new_client->client_addr, &sockaddr_len);

			if (new_client->socket_descriptor == INVALID_SOCKET) {
				printf("Server: TCP accept failed. Error code: %i\n", get_socket_error());
				socket_cleanup();
				exit(1);
			}

			// printf("Server: TCP accpeting incoming client\n");
			struct operating_parameters *params_buffer = (struct operating_parameters *) malloc(sizeof(struct operating_parameters));

			retstat = recv(new_client->socket_descriptor, (char *) params_buffer, sizeof(struct operating_parameters), 0);
			if (retstat > 0 && next_client < max_client) {
				new_client->connection_type = params_buffer->connection_type;
				new_client->connection_mode = params_buffer->connection_mode;
				new_client->packet_size = params_buffer->packet_size;
				new_client->packet_rate = params_buffer->packet_rate;
				new_client->packet_number = params_buffer->packet_number;
				new_client->next_packet_number = 0;

				// debug_args(new_client);
				connecting_client[next_client] = new_client;
				next_client += 1;
				tcp_client += 1;
				// printf("Server: Current connecting client %d\n", next_client);
				
				// Receive from Client
				if (new_client->connection_mode == true) {
					FD_SET(new_client->socket_descriptor, &socket_read_set);
				}
				// Send to Client
				else {
					if (new_client->packet_rate > 0)
						new_client->time_interval = 1000 * new_client->packet_size / new_client->packet_rate;
					else
						new_client->time_interval = 0;
					new_client->next_packet_transmission_time = timer.Elapsed() + new_client->time_interval;

					FD_SET(new_client->socket_descriptor, &socket_write_set);
				}
			}
			else if (next_client >= max_client)
				printf("Server: maximum number of clients reached\n");
			else if (retstat != sizeof(params_buffer))
				printf("Server: operating parameters missing.\n");
			else
				printf("Server: recv failed. Error code: %i\n", get_socket_error());
			
			free(params_buffer);
		}

		// UDP socket is available to read
        if (FD_ISSET(udp_socket, &socket_read_set)) {
			struct sockaddr_in *client_info = (struct sockaddr_in *) malloc(sizeof(struct sockaddr_in));
			socklen_t sockaddr_len = sizeof(struct sockaddr_in);
			
			// printf("Server: UDP accpeting incoming client\n");

			struct operating_parameters *params_buffer = (struct operating_parameters *) malloc(sizeof(struct operating_parameters));
			
			retstat = recvfrom(udp_socket, (char *) params_buffer, sizeof(struct operating_parameters), 0,(struct sockaddr *) client_info, &sockaddr_len); 

			if (retstat > 0 || get_socket_error() == WSAEMSGSIZE) {
				// Find connecting client
				int client_index = -1;
				for (int i = 0; i < next_client; i++) {
					if (sockaddr_equal(&connecting_client[i]->client_addr, client_info) == true) {
						client_index = i;
						break;
					}
				}

				if (client_index == -1 && next_client < max_client) {
					struct incoming_client *new_client = (struct incoming_client *) malloc(sizeof(struct incoming_client));
					memset(&new_client->client_addr, 0, sizeof(struct sockaddr_in));

					new_client->client_addr.sin_family = client_info->sin_family;
					new_client->client_addr.sin_addr = client_info->sin_addr;
					new_client->client_addr.sin_port = client_info->sin_port;

					new_client->socket_descriptor = udp_socket;
					new_client->connection_type = params_buffer->connection_type;
					new_client->connection_mode = params_buffer->connection_mode;
					new_client->packet_size = params_buffer->packet_size;
					new_client->packet_rate = params_buffer->packet_rate;
					new_client->packet_number = params_buffer->packet_number;

					// debug_args(new_client);

					connecting_client[next_client] = new_client;
					next_client += 1;
					udp_client += 1;

					// UDP server starts to initialize sending packets to client
					if (new_client->connection_mode == false) {
						new_client->next_packet_number = 0;
						if (new_client->packet_rate > 0)
							new_client->time_interval = 1000 * new_client->packet_size / new_client->packet_rate;
						else
							new_client->time_interval = 0;
						new_client->next_packet_transmission_time = timer.Elapsed() + new_client->time_interval;

						// printf("Next packet time: %ld\n", new_client->next_packet_transmission_time);
						FD_SET(udp_socket, &socket_write_set);
						// printf("Server: Current connecting client %d\n", next_client);
					}
				}
				// UDP client keeps sending packets to server
				else if (client_index != -1) {
					// printf("Server: Client %d keeps connecting\n", client_index);

					char *recv_buffer = (char *) params_buffer;
					connecting_client[client_index]->next_packet_number = atoi(recv_buffer) + 1;
					// printf("Server: Next packet number %ld\n", connecting_client[client_index]->next_packet_number);

					// Remove from connection since packets are all delivered
					if (connecting_client[client_index]->next_packet_number == connecting_client[client_index]->packet_number) {
						free(connecting_client[client_index]);
						for (int i = client_index + 1; i < max_client; i++)
							connecting_client[i - 1] = connecting_client[i];
						next_client -= 1;
						udp_client -= 1;
					}

					// printf("Server: Current connecting client %d\n", next_client);
				}
				else if (next_client >= max_client)
					printf("Server: maximum number of clients reached\n");
			}
			else
				printf("Server: recvfrom failed. Error code: %i\n", get_socket_error());
			
			free(params_buffer);
			free(client_info);
		}

		// UDP socket is available to write
		if (FD_ISSET(udp_socket, &socket_write_set)) {
			// Compare with every connected timer and check any packet to send
			int client_index = -1;
			for (int i = 0; i < next_client; i++) {
				if (connecting_client[i]->connection_mode == false && connecting_client[i]->connection_type == false) {
					// Compare current time and next sending time
					long current_time = timer.Elapsed();
					if (current_time >= connecting_client[i]->next_packet_transmission_time) {
						char *sendbuf = (char *)calloc(connecting_client[i]->packet_size, sizeof(char));
						memset(sendbuf, 0, connecting_client[i]->packet_size);
						sprintf(sendbuf, "%ld", connecting_client[i]->next_packet_number);

						int iResult = sendto(udp_socket, sendbuf, connecting_client[i]->packet_size, 0, (struct sockaddr *) &connecting_client[i]->client_addr, sizeof(struct sockaddr_in));
						if (iResult == SOCKET_ERROR) {
							printf("Server: sendto failed. Error code: %i\n", get_socket_error());
						}
						// printf("Server: Send packet to client %d with packet number %ld\n", i, connecting_client[i]->next_packet_number);

						current_time = timer.Elapsed();
						long delay_sent = current_time - connecting_client[i]->next_packet_transmission_time;

						connecting_client[i]->next_packet_transmission_time += (connecting_client[i]->time_interval - delay_sent);
						connecting_client[i]->next_packet_number += 1;
						free(sendbuf);

						// Remove from connection since packets are all delivered
						if (connecting_client[i]->next_packet_number == connecting_client[i]->packet_number) {
							free(connecting_client[i]);
							for (int j = i + 1; j < max_client; j++)
								connecting_client[j - 1] = connecting_client[j];
							next_client -= 1;
							udp_client -= 1;

							// printf("Server: Current connecting client %d\n", next_client);
							break;
						}
					}
				}
			}
		}

		// Check all TCP sockets
		for (int i = 0; i < next_client; i++) {
			if (connecting_client[i]->connection_type == true) {
				// Receive message from TCP client
				if (connecting_client[i]->connection_mode == true) {
					if (FD_ISSET(connecting_client[i]->socket_descriptor, &socket_read_set)) {
						bool close_connection = false;
						// printf("Server: Client %d keeps connecting\n", i);

						char *recv_buffer = (char *) calloc(sizeof(char), connecting_client[i]->packet_size);
						int retstat = recv(connecting_client[i]->socket_descriptor, recv_buffer, connecting_client[i]->packet_size, 0);

						if (retstat > 0) {
							connecting_client[i]->next_packet_number = atoi(recv_buffer) + 1;
							// printf("Server: Next packet number %ld\n", connecting_client[i]->next_packet_number);
						}
						else if (retstat == 0) {
							retstat = shutdown(connecting_client[i]->socket_descriptor, SD_SEND);
							if (retstat == SOCKET_ERROR) {
								printf("Server: shutdown failed. Error code: %i\n", get_socket_error());
							}
							close_connection = true;
						}
						free(recv_buffer);

						// Remove from connection since packets are all delivered
						if (connecting_client[i]->next_packet_number == connecting_client[i]->packet_number || close_connection == true) {
							FD_CLR(connecting_client[i]->socket_descriptor, &socket_read_set);

							free(connecting_client[i]);
							for (int j = i + 1; j < max_client; j++)
								connecting_client[j - 1] = connecting_client[j];
							next_client -= 1;
							tcp_client -= 1;
							
							// printf("Server: Current connecting client %d\n", next_client);
							break;
						}
					}
				}
				// Send message to TCP client
				else {
					if (FD_ISSET(connecting_client[i]->socket_descriptor, &socket_write_set)) {
						long current_time = timer.Elapsed();
						if (current_time >= connecting_client[i]->next_packet_transmission_time) {
							char *sendbuf = (char *)calloc(connecting_client[i]->packet_size, sizeof(char));
							memset(sendbuf, 0, connecting_client[i]->packet_size);
							sprintf(sendbuf, "%ld", connecting_client[i]->next_packet_number);

							int iResult = send(connecting_client[i]->socket_descriptor, sendbuf, connecting_client[i]->packet_size, 0);
							if (iResult == SOCKET_ERROR) {
								printf("Server: send failed. Error code: %i\n", get_socket_error());
							}
							// printf("Server: Send packet to client %d with packet number %ld\n", i, connecting_client[i]->next_packet_number);

							current_time = timer.Elapsed();
							long delay_sent = current_time - connecting_client[i]->next_packet_transmission_time;

							connecting_client[i]->next_packet_transmission_time += (connecting_client[i]->time_interval - delay_sent);
							connecting_client[i]->next_packet_number += 1;
							free(sendbuf);

							// Remove from connection since packets are all delivered
							if (connecting_client[i]->next_packet_number == connecting_client[i]->packet_number) {
								retstat = shutdown(connecting_client[i]->socket_descriptor, SD_SEND);
								if (retstat == SOCKET_ERROR) {
									printf("Server: shutdown failed. Error code: %i\n", get_socket_error());
								}

								FD_CLR(connecting_client[i]->socket_descriptor, &socket_write_set);
								
								free(connecting_client[i]);
								for (int j = i + 1; j < max_client; j++)
									connecting_client[j - 1] = connecting_client[j];
								next_client -= 1;
								tcp_client -= 1;

								// printf("Server: Current connecting client %d\n", next_client);
								break;
							}
						}
					}
				}
			}
		}

	}
}

int main(int argc, char **argv) {
	timer.Start();

	args_parser(argc, argv);

	connecting_client = (struct incoming_client **) malloc(sizeof(struct incoming_client *) * max_client);

	if (!hostname) {
		hostname = (char *)calloc(strlen("IN_ADDR_ANY") + 1, sizeof(char));
		strcpy(hostname, "IN_ADDR_ANY");
	}

	// Start the child thread
	thread thread_id(statistics_display, NULL);

	socket_init();
	concurrent_client();

	// Wait for the thread to finish
	thread_id.join();

	if (hostname != NULL)
		free(hostname);
	
	return 0;
}