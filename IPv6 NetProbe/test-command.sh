#!/bin/sh

echo "TCP recv Dualstack"
./client.o -recv -proto tcp -pktsize 1000 -pktrate 1000 -pktnum 10

sleep 1

echo "TCP send Dualstack"
./client.o -send -proto tcp -pktsize 1000 -pktrate 1000 -pktnum 10

sleep 1

echo "UDP recv Dualstack"
./client.o -recv -proto udp -pktsize 1000 -pktrate 1000 -pktnum 10

sleep 1

echo "UDP send Dualstack"
./client.o -send -proto udp -pktsize 1000 -pktrate 1000 -pktnum 10

sleep 1

echo "TCP recv IPv4"
./client.o -recv -proto tcp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv4

sleep 1

echo "TCP send IPv4"
./client.o -send -proto tcp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv4

sleep 1

echo "UDP recv IPv4"
./client.o -recv -proto udp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv4

sleep 1

echo "UDP send IPv4"
./client.o -send -proto udp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv4

sleep 1

echo "TCP recv IPv6"
./client.o -recv -proto tcp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv6

sleep 1

echo "TCP send IPv6"
./client.o -send -proto tcp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv6

sleep 1

echo "UDP recv IPv6"
./client.o -recv -proto udp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv6

sleep 1

echo "UDP send IPv6"
./client.o -send -proto udp -pktsize 1000 -pktrate 1000 -pktnum 10 -ipv6
