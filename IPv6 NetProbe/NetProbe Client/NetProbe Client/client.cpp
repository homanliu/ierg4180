#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifdef _WIN32
	// Print console from thread
	#include <tchar.h>
	#include <strsafe.h>

	// Socket programming
	#include <WinSock2.h>
	#include <WS2tcpip.h>

    // Option parsing
	#include "getopt.h"

	// Need to link with Ws2_32.lib
	#pragma comment (lib, "Ws2_32.lib")

    #define get_socket_error() WSAGetLastError()
#else // Assume Linux
	#include <sys/types.h>
	#include <sys/socket.h>
	#include <sys/ioctl.h>
	#include <sys/fcntl.h>
	#include <netinet/in.h>
	#include <netinet/tcp.h>
	#include <arpa/inet.h>
	#include <netdb.h>
	#include <unistd.h>
	#include <errno.h>

	#include <getopt.h>

	// Function rename
	#define get_socket_error() (errno)
	#define closesocket(s) close(s)
	#define Sleep(x) usleep(x * 1000)

    // Variable rename
	#define SOCKET int
	#define INVALID_SOCKET -1
	#define SOCKET_ERROR -1
	#define SD_SEND SHUT_WR
#endif

#include "tinythread.h"
#include "es_timer.h"

using namespace std;
using namespace tthread;

#define HOST_INFO 3
#define RESPONSE_MODE 2
#define SEND_MODE 1
#define RECV_MODE 0

// Configuration
char *hostname = NULL, protocol[4] = "UDP";
int port_number = 4180, stat_update = 500, mode = SEND_MODE;
int pktsize = 1000, pktrate = 1000, pktnum = 0;
int rbufsize = 0, sbufsize = 0;

// Socket variables
struct sockaddr_in sock_addr;
struct sockaddr_in6 sock6_addr;
SOCKET socket_descriptor;

// Thread variables
int keep_running = 1;

// Statistics Display
long elapsed_time, packet_received = 0, packet_lost = 0;
double throughput = 0, jitter = 0;
long accumulate_filesize[3] = { 0, 0, 0 };
double jitter_old = 0, jitter_new = 0;
ES_Timer program_timer = ES_Timer();

// Configuration struct
struct operating_parameters {
	// TCP = true, UDP = false
	bool connection_type = false;
	// Server receive = true, Server send = false
	bool connection_mode = false;

	int packet_size = 0;
	int packet_rate = 0;
	long packet_number = 0;
};
struct operating_parameters *client_params;

// Response mode
double min_response = 1000000.0, max_response = 0.0;
long total_response = 0;
double average_response = 0.0;

// IPv6
int ipv6_enable = 0;

long expected_sequence = 0;

void args_parser(int argc, char **argv) {
	const char *short_opt = "ab:c:d:e:f:g:h:i:jk:mpqz:";
	struct option long_opt[] =
	{
		{ "send",     no_argument,       NULL, 'a' },
		{ "recv",     no_argument,       NULL, 'j' },
		{ "response", no_argument,       NULL, 'm' },
		{ "host", 	  required_argument, NULL, 'z' },

		{ "stat",     required_argument, NULL, 'b' },

		{ "rhost",    required_argument, NULL, 'c' },

		{ "rport",    required_argument, NULL, 'd' },

		{ "proto",    required_argument, NULL, 'e' },

		{ "pktsize",  required_argument, NULL, 'f' },

		{ "pktrate",  required_argument, NULL, 'g' },

		{ "pktnum",   required_argument, NULL, 'h' },

		{ "sbufsize", required_argument, NULL, 'i' },
		{ "rbufsize", required_argument, NULL, 'k' },

		{ "ipv4", no_argument, NULL, 'p'},
		{ "ipv6", no_argument, NULL, 'q'},

		{ NULL,       0,                 NULL,  0 }
	};

	int retstat;

	while ((retstat = getopt_long_only(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (retstat) {
		case -1:       /* no more arguments */
		case 0:        /* long options toggles */
			break;

			// Sending mode
		case 'a':
			mode = SEND_MODE;
			break;

			// Receiving mode
		case 'j':
			mode = RECV_MODE;
			break;
		
			// Response mode
		case 'm':
			mode = RESPONSE_MODE;
			break;
		
		case 'z':
			mode = HOST_INFO;
			hostname = (char *)calloc(strlen(optarg) + 1, sizeof(char));
			strcpy(hostname, optarg);
			break;

			// Update of statistics display
		case 'b':
			stat_update = atoi(optarg);
			break;

			// Hostname
		case 'c':
			hostname = (char *)calloc(strlen(optarg) + 1, sizeof(char));
			strcpy(hostname, optarg);
			break;

			// Port number
		case 'd':
			port_number = atoi(optarg);
			break;

			// Protocol
		case 'e':
			if (strcmp(optarg, "TCP") == 0 || strcmp(optarg, "tcp") == 0)
				strcpy(protocol, "TCP");
			break;

			// Packet size
		case 'f':
			pktsize = atoi(optarg);
			break;

			// Packet rate
		case 'g':
			pktrate = atoi(optarg);
			break;

			// Packet number
		case 'h':
			pktnum = atoi(optarg);
			break;

			// Buffer size
		case 'i':
			sbufsize = atoi(optarg);
			break;

		case 'k':
			rbufsize = atoi(optarg);
			break;
		
		case 'p':
			ipv6_enable = -1;
			break;
		
		case 'q':
			ipv6_enable = 1;
			break;
		};
	}
}

void debug_args() {
	printf("Input Parameters: \n");
	printf("Statistics update: %d\n", stat_update);
	printf("Hostname: %s\n", hostname);
	printf("Port number: %d\n", port_number);
	printf("Protocol: %s\n", protocol);
	printf("Packet size: %d\n", pktsize);
	printf("Packet rate: %d\n", pktrate);
	printf("Packet number: %d\n", pktnum);
	printf("==================================\n\n");
}

// Later for statistics display in multi-threading
void displayThread(void *data) {
	long duration = 0;
	ES_Timer timer = ES_Timer();
	timer.Start();

	while (keep_running != 0) {
		Sleep(stat_update);
		duration = timer.Elapsed();

		double txrate = accumulate_filesize[0] * 8000 / duration;
		txrate += accumulate_filesize[1] * 8 / duration;
		txrate += accumulate_filesize[2] * 8 / (1000 * duration);

		// printf("Accumulated Filesize: %ldMB, %ldKB, %ldB\n", accumulate_filesize[0], accumulate_filesize[1], accumulate_filesize[2]);

		if (mode == RECV_MODE) {
			double lost_percentage = packet_lost * 100.0 / packet_received;
			if (packet_received == 0)
				lost_percentage = 0;

			printf("Elapsed [%ldms] Pkts [%ld] Lost [%ld, %.2f%%] Rate [%.2fMbps] Jitter [%.2fms]\n",
				duration,
				packet_received,
				packet_lost,
				lost_percentage,
				txrate,
				jitter_new);
		}
		else if (mode == SEND_MODE) {
			printf("Elapsed [%ldms] Rate [%.2fMbps]\n",
				duration,
				txrate);
		}
		else if (mode == RESPONSE_MODE) {
			printf("Elapsed [%ldms] Replies [%ld] Min [%.4fms] Max [%.4fms] Avg [%.4fms] Jitter [%.4fms]\n",
				duration,
				total_response,
				min_response,
				max_response,
				average_response,
				jitter_new
			);
		}
	}
	return;
}
// ===============================================

void socket_cleanup() {
	// struct linger optval;
	// optval.l_onoff = 1;
	// optval.l_linger = 5;
	// setsockopt(socket_descriptor, SOL_SOCKET, SO_LINGER, (char *)&optval, sizeof(optval));
	if (mode != RESPONSE_MODE)
		keep_running = 0;

	if (socket_descriptor != INVALID_SOCKET) {
		closesocket(socket_descriptor);
	}

	#ifdef _WIN32
		WSACleanup();
	#endif
}

void socket_init() {
	int iResult;
	#ifdef _WIN32
		WSADATA wsaData;
		iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0) {
			printf("WSAStartup failed: %d\n", iResult);
			exit(1);
		}
	#endif

	memset(&sock_addr, 0, sizeof(struct sockaddr_in));
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(port_number);

	// Resolve hostname to address
	sock_addr.sin_addr.s_addr = inet_addr(hostname);
	if (sock_addr.sin_addr.s_addr == -1) {
		struct hostent *host_result = gethostbyname(hostname);
		if (host_result != NULL) {
			// Debug IP address
			struct in_addr addr = { 0, };
			addr.s_addr = *(u_long *)host_result->h_addr_list[0];
			printf("Resolved IP Address: %s\n", inet_ntoa(addr));

			sock_addr.sin_addr.s_addr = inet_addr(inet_ntoa(addr));
		}
	}
	// ============================

	// Resolve hostname to IPv6 address
	memset(&sock6_addr, 0, sizeof(struct sockaddr_in6));
	sock6_addr.sin6_family = AF_INET6;
	sock6_addr.sin6_port = htons(port_number);

	if (ipv6_enable != -1) {
		struct addrinfo hints, *res, *p;
		int status;
		memset(&hints, 0, sizeof hints);
		hints.ai_family = AF_INET6;
		if ((status = getaddrinfo(hostname, NULL, &hints, &res)) != 0) {
			if (ipv6_enable == 0)
				ipv6_enable = -1;

			if (ipv6_enable == 1) {
				fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
				exit(1);
			}
		}
		else {
			ipv6_enable = 1;
		}

		if (ipv6_enable == 1) {
			struct sockaddr_in6 *ipv6 = (struct sockaddr_in6 *)res->ai_addr;
			char str[INET6_ADDRSTRLEN];

			sock6_addr.sin6_addr = ipv6->sin6_addr;
			inet_ntop(AF_INET6, &(ipv6->sin6_addr), str, INET6_ADDRSTRLEN);
			printf("Resolved IPv6 Address: %s\n", str);

			freeaddrinfo(res);
		}
	}
	// ============================

	// Create socket descriptor
	if (ipv6_enable == 1) {
		if (strcmp(protocol, "TCP") == 0)
			socket_descriptor = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
		else
			socket_descriptor = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
	}
	else {
		if (strcmp(protocol, "TCP") == 0)
			socket_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		else
			socket_descriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	}
	
	if (socket_descriptor == INVALID_SOCKET) {
		printf("Client: socket failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}
	// ========================
	
	// Connect to server
	if (ipv6_enable == 1)
		iResult = connect(socket_descriptor, (struct sockaddr *) &sock6_addr, sizeof(struct sockaddr_in6));
	else
		iResult = connect(socket_descriptor, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));

	if (iResult == SOCKET_ERROR) {
		printf("Client: connect() failed. Error code: %i\n", get_socket_error());
		socket_cleanup();
		exit(1);
	}
	// =================

	// Set send and receive buffer size
	if (sbufsize > 0) {
		iResult = setsockopt(socket_descriptor, SOL_SOCKET, SO_SNDBUF, (char *)&sbufsize, sizeof(sbufsize));
		if (iResult < 0) {
			printf("Server: set socket sbufsize error. Error code: %i\n", get_socket_error());
			socket_cleanup();
			exit(1);
		}
	}
	if (rbufsize > 0) {
		iResult = setsockopt(socket_descriptor, SOL_SOCKET, SO_RCVBUF, (char *)&rbufsize, sizeof(rbufsize));
		if (iResult < 0) {
			printf("Server: set socket rbufsize error. Error code: %i\n", get_socket_error());
			socket_cleanup();
			exit(1);
		}
	}
	// ==================================
}

void operating_parameters() {
	client_params = (struct operating_parameters *) malloc(sizeof(struct operating_parameters));

	// Put congiuration into structure
	client_params->connection_type = (strcmp(protocol, "TCP") == 0);
	if (mode == RESPONSE_MODE) {
		client_params->connection_mode = RECV_MODE;
		client_params->packet_rate = htonl(0);
		client_params->packet_number = htonl(1);
	}
	else {
		client_params->connection_mode = mode;
		client_params->packet_rate = htonl(pktrate);
		client_params->packet_number = htonl(pktnum);
	}
	client_params->packet_size = htonl(pktsize);
	// ===============================

	// Send to server and initialize connection
	int iResult;
	if (strcmp(protocol, "TCP") == 0)
		iResult = send(socket_descriptor, (char *) client_params, sizeof(struct operating_parameters), 0);
	else {
		if (ipv6_enable == 1)
			iResult = sendto(socket_descriptor, (char *) client_params, sizeof(struct operating_parameters), 0, (struct sockaddr *) &sock6_addr, sizeof(struct sockaddr_in6));
		else
			iResult = sendto(socket_descriptor, (char *) client_params, sizeof(struct operating_parameters), 0, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));
	}
	// ========================================

	if (iResult == SOCKET_ERROR) {
		printf("Client: send failed. Error code: %i\n", get_socket_error());
	}

	free(client_params);
}

void sending_mode() {
	// printf("Sending Mode\n");
	// debug_args();

	int iResult;
	long sent_count = 0;
	char *sendbuf = (char *)calloc(pktsize, sizeof(char));
	ES_Timer timer = ES_Timer();

	while (sent_count < pktnum || pktnum == 0) {
		// printf("Packet number: %ld\n", sent_count);
		memset(sendbuf, 0, pktsize);
		sprintf(sendbuf, "%ld", sent_count);

		timer.Start();
		if (strcmp(protocol, "TCP") == 0) {
			iResult = send(socket_descriptor, sendbuf, pktsize, 0);
		}
		else {
			if (ipv6_enable == 1)
				iResult = sendto(socket_descriptor, sendbuf, pktsize, 0, (struct sockaddr *) &sock6_addr, sizeof(struct sockaddr_in6));
			else
				iResult = sendto(socket_descriptor, sendbuf, pktsize, 0, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));
		}
		double duration = timer.Elapsed();

		if (iResult == SOCKET_ERROR) {
			printf("Client: send failed. Error code: %i\n", get_socket_error());
			break;
		}
		else {
			// printf("Client: send packet with size %d and packet number %ld\n", iResult, sent_count);
			accumulate_filesize[2] += (iResult);
			if (accumulate_filesize[2] > 1024) {
				accumulate_filesize[1] += (accumulate_filesize[2] / 1024);
				accumulate_filesize[2] = accumulate_filesize[2] % 1024;
			}
			if (accumulate_filesize[1] > 1024) {
				accumulate_filesize[0] += (accumulate_filesize[1] / 1024);
				accumulate_filesize[1] = accumulate_filesize[1] % 1024;
			}
		}

		if (pktrate > 0) {
			double sleep_time = (1000.0 / pktrate) * iResult - duration;
			if (sleep_time > 0)
				Sleep(sleep_time);
		}

		sent_count += 1;
	}

	free(sendbuf);
	socket_cleanup();
}

void receiving_mode() {
	// printf("Receiving Mode\n");
	// debug_args();

	int iResult, buflen = pktsize;
	char *recvbuf;
	struct sockaddr *sender_addr;
	recvbuf = (char *)calloc(buflen, sizeof(char));

	int sockaddr_len;
	if (ipv6_enable == 1) {
		sender_addr = (struct sockaddr *) calloc(1, sizeof(struct sockaddr_in6));
		sockaddr_len = sizeof(struct sockaddr_in6);
	}
	else {
		sender_addr = (struct sockaddr *) calloc(1, sizeof(struct sockaddr_in));
		sockaddr_len = sizeof(struct sockaddr_in);
	}

	long incoming_time, previous_time = 0;
	do {
		if (strcmp(protocol, "TCP") == 0) {
			iResult = recv(socket_descriptor, recvbuf, buflen, 0);
		}
		else {
			iResult = recvfrom(socket_descriptor, recvbuf, buflen, 0, (struct sockaddr *) sender_addr, (socklen_t *) &sockaddr_len);
		}

		if (iResult == 0) {
			printf("Client: connection closing...\n");
			break;
		}
		else if (iResult == SOCKET_ERROR || iResult < 0) {
			printf("Client: recv failed. Error code: %i\n", get_socket_error());
			break;
		}
		else {
			incoming_time = program_timer.Elapsed();
			packet_received += 1;
			accumulate_filesize[2] += (iResult);
			if (accumulate_filesize[2] > 1024) {
				accumulate_filesize[1] += (accumulate_filesize[2] / 1024);
				accumulate_filesize[2] = accumulate_filesize[2] % 1024;
			}
			if (accumulate_filesize[1] > 1024) {
				accumulate_filesize[0] += (accumulate_filesize[1] / 1024);
				accumulate_filesize[1] = accumulate_filesize[1] % 1024;
			}

			double time_T = (double) (incoming_time - previous_time) / packet_received;
			jitter_new = (jitter_old * (packet_received - 1) + (incoming_time - previous_time - time_T)) / packet_received;

			jitter_old = jitter_new;
			previous_time = incoming_time;

			long sequence_number = atoi(recvbuf);
			// printf("Client: received packet with sequence number %ld and packet size %d\n", sequence_number, iResult);
			// printf("Client: expected sequence number %ld\n", expected_sequence);

			if (sequence_number > expected_sequence) {
				packet_lost += (sequence_number - expected_sequence);
				expected_sequence = sequence_number + 1;
			}
			
			expected_sequence += 1;
			// printf("Client: expected sequence number %ld\n", expected_sequence);

			if (expected_sequence == pktnum)
				break;
		}
	} while (iResult > 0);

	if (strcmp(protocol, "TCP") == 0) {
		iResult = shutdown(socket_descriptor, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			printf("Client: shutdown failed. Error code: %i\n", get_socket_error());
		}
	}

	free(recvbuf);
	socket_cleanup();
}

void response_mode() {
	if (strcmp(protocol, "TCP") != 0) {
		printf("Client: Response mode is only available for TCP.\n");
		exit(1);
	}
	if (pktrate == 1000)
		pktrate = 10;

	ES_Timer timer = ES_Timer();
	int iResult, buflen = pktsize;
	double response_time;
	char *recvbuf;

	while (total_response < pktnum || pktnum == 0) {
		// Create new socket
		socket_init();

		operating_parameters();
		response_time = timer.ElapseduSec();
		long duration = timer.Elapsed();

		recvbuf = (char *)calloc(buflen, sizeof(char));
		iResult = recv(socket_descriptor, recvbuf, buflen, 0);

		if (iResult == 0) {
			printf("Client: connection closing...\n");
		}
		else if (iResult == SOCKET_ERROR || iResult < 0) {
			printf("Client: recv failed. Error code: %i\n", get_socket_error());
			break;
		}
		else {
			response_time = timer.ElapseduSec() - response_time;
			response_time *= 0.001;

			max_response = (response_time > max_response) ? response_time : max_response;
			min_response = (response_time < min_response) ? response_time : min_response;
			average_response = (average_response * total_response + response_time) / (total_response + 1);

			double jitter_average = fabs(response_time - average_response);
			if (total_response == 0)
				jitter_new = response_time;
			else
				jitter_new = (jitter_new * total_response + jitter_average) / (total_response + 1);

			total_response += 1;

			if (pktrate > 0) {
				double sleep_time = (1000.0 / pktrate) * total_response - duration;
				if (sleep_time > 0)
					Sleep(sleep_time);
			}
		}
		
		socket_cleanup();
	}
	keep_running = 0;
}

void host_info() {
	printf("Host Information Mode\n");
	printf("=======================================\n\n");
	printf("Hostname: %s\n\n", hostname);
	// debug_args();

	struct addrinfo hints, *res, *p;
	int status;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_socktype = SOCK_STREAM;
	if ((status = getaddrinfo(hostname, NULL, &hints, &res)) != 0) {
		if (ipv6_enable >= 0) {
			fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
			exit(1);
		}
	}

	struct sockaddr_in *ipv4;
	struct sockaddr_in6 *ipv6;
	char ipv4_str[INET_ADDRSTRLEN];
	char ipv6_str[INET6_ADDRSTRLEN];

	while (res) {

		switch(res->ai_family) {
			case AF_INET:
				ipv4 = (struct sockaddr_in *)res->ai_addr;
				inet_ntop(AF_INET, &(ipv4->sin_addr), ipv4_str, INET_ADDRSTRLEN);
				printf("Resolved IPv4 Address: %s\n\n", ipv4_str);
				break;
			
			case AF_INET6:
				ipv6 = (struct sockaddr_in6 *)res->ai_addr;
				inet_ntop(AF_INET6, &(ipv6->sin6_addr), ipv6_str, INET6_ADDRSTRLEN);
				printf("Resolved IPv6 Address: %s\n\n", ipv6_str);
				break;
		}

		res = res->ai_next;
	}

	freeaddrinfo(res);
}

int main(int argc, char **argv) {
	args_parser(argc, argv);

	if (!hostname) {
		hostname = (char *)calloc(strlen("localhost") + 1, sizeof(char));
		strcpy(hostname, "localhost");
	}

	if (mode == SEND_MODE || mode == RECV_MODE) {
		socket_init();
		operating_parameters();

		// Start the child thread
		thread thread_id(displayThread, NULL);

		if (mode == SEND_MODE)
			sending_mode();
		else
			receiving_mode();
		
		// Wait for the thread to finish
		thread_id.join();
	}
	else if (mode == RESPONSE_MODE) {
		// Start the child thread
		thread thread_id(displayThread, NULL);

		response_mode();
		
		// Wait for the thread to finish
		thread_id.join();
	}
	else {
		host_info();
	}

	if (hostname != NULL)
		free(hostname);

	return 0;
}