#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

// Print console from thread
#include <tchar.h>
#include <strsafe.h>

// Socket programming
#include <WinSock2.h>
#include <WS2tcpip.h>

// Option parsing
#include "getopt.h"
#include "es_timer.h"

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#define SEND_MODE 1
#define RECV_MODE -1
#define HOST_INFO 0

#define SERVER_MODE RECV_MODE
#define CLIENT_MODE SEND_MODE

// Configuration
char *hostname = NULL, protocol[4] = "UDP";
int port_number, stat_update, pktsize, pktrate, pktnum, bufsize, mode;

// Socket variables
struct sockaddr_in sock_addr, sender_addr;
SOCKET socket_descriptor, connector;

// Thread variables
int keep_running = 1;
HANDLE display_thread;

// Statistics Display
long elapsed_time, packet_received = 0, packet_lost = 0;
double throughput = 0, jitter = 0;
long accumulate_filesize[3] = { 0, 0, 0 };
double jitter_old = 0, jitter_new = 0;
ES_Timer program_timer = ES_Timer();

void args_parser(int argc, char **argv) {
	const char *short_opt = "ab:c:d:e:f:g:h:i:jk";
	struct option long_opt[] =
	{
		{ "send",     no_argument,       NULL, 'a' },
		{ "recv",     no_argument,       NULL, 'j' },
		{ "host",     no_argument,       NULL, 'k' },

		{ "stat",     required_argument, NULL, 'b' },

		{ "rhost",    required_argument, NULL, 'c' },
		{ "lhost",    required_argument, NULL, 'c' },

		{ "rport",    required_argument, NULL, 'd' },
		{ "lport",    required_argument, NULL, 'd' },

		{ "proto",    required_argument, NULL, 'e' },

		{ "pktsize",  required_argument, NULL, 'f' },

		{ "pktrate",  required_argument, NULL, 'g' },

		{ "pktnum",   required_argument, NULL, 'h' },

		{ "sbufsize", required_argument, NULL, 'i' },
		{ "rbufsize", required_argument, NULL, 'i' },

		{ NULL,       0,                 NULL,  0 }
	};

	int retstat;

	while ((retstat = getopt_long_only(argc, argv, short_opt, long_opt, NULL)) != -1) {
		switch (retstat) {
		case -1:       /* no more arguments */
		case 0:        /* long options toggles */
			break;

			// Sending mode
		case 'a':
			mode = SEND_MODE;
			break;

			// Receiving mode
		case 'j':
			mode = RECV_MODE;
			break;

			// Host information mode
		case 'k':
			mode = HOST_INFO;
			break;

			// Update of statistics display
		case 'b':
			stat_update = atoi(optarg);
			break;

			// Hostname
		case 'c':
			hostname = (char *)calloc(strlen(optarg) + 1, sizeof(char));
			strcpy(hostname, optarg);
			break;

			// Port number
		case 'd':
			port_number = atoi(optarg);
			break;

			// Protocol
		case 'e':
			if (strcmp(optarg, "TCP") == 0 || strcmp(optarg, "tcp") == 0)
				strcpy(protocol, "TCP");
			break;

			// Packet size
		case 'f':
			pktsize = atoi(optarg);
			break;

			// Packet rate
		case 'g':
			pktrate = atoi(optarg);
			break;

			// Packet number
		case 'h':
			pktnum = atoi(optarg);
			break;

			// Buffer size
		case 'i':
			bufsize = atoi(optarg);
			break;
		};
	}
}

void init_netprobe() {
	// Initialize port number
	port_number = 4180;

	// Statistis display (ms)
	stat_update = 500;

	pktsize = 1000;
	pktrate = 1000;
	pktnum = 0;
	bufsize = 0;
}

void free_netprobe() {
	if (hostname != NULL) {
		free(hostname);
	}
}

void debug_args() {
	printf("Input Parameters: \n");
	printf("Statistics update: %d\n", stat_update);
	printf("Hostname: %s\n", hostname);
	printf("Port number: %d\n", port_number);
	printf("Protocol: %s\n", protocol);
	printf("Packet size: %d\n", pktsize);
	printf("Packet rate: %d\n", pktrate);
	printf("Packet number: %d\n", pktnum);
	printf("Buffer size: %d\n", bufsize);
	printf("==================================\n\n");
}

DWORD WINAPI displayThread(void *data) {
	long duration = 0;
	ES_Timer timer = ES_Timer();
	timer.Start();

	while (keep_running != 0) {
		Sleep(stat_update);
		duration = timer.Elapsed();

		double txrate = accumulate_filesize[0] * 8000 / duration;
		txrate += accumulate_filesize[1] * 8 / duration;
		txrate += accumulate_filesize[2] * 8 / (1000 * duration);

		if (mode == RECV_MODE) {
			double lost_percentage = packet_lost * 100.0 / packet_received;
			if (packet_received == 0)
				lost_percentage = 0;

			printf("Elapsed [%dms] Pkts [%d] Lost [%d, %.2f%] Rate [%.2fMbps] Jitter [%.2fms]\n",
				duration,
				packet_received,
				packet_lost,
				lost_percentage,
				txrate,
				jitter_new);
		}
		else if (mode == SEND_MODE) {
			printf("Elapsed [%dms] Rate [%.2fMbps]\n",
				duration,
				txrate);
		}
	}
	return 0;
}

void socket_cleanup() {
	keep_running = 0;
	WaitForSingleObject(display_thread, INFINITE);

	struct linger optval;
	optval.l_onoff = 1;
	optval.l_linger = 5;
	setsockopt(socket_descriptor, SOL_SOCKET, SO_LINGER, (char *)&optval, sizeof(optval));

	if (connector != INVALID_SOCKET) {
		closesocket(connector);
	}

	if (socket_descriptor != INVALID_SOCKET) {
		closesocket(socket_descriptor);
	}

	WSACleanup();
}

void socket_init() {
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed: %d\n", iResult);
		exit(1);
	}

	memset(&sock_addr, 0, sizeof(struct sockaddr_in));
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(port_number);

	// Resolve hostname to address
	if (strcmp(hostname, "IN_ADDR_ANY") == 0) {
		sock_addr.sin_addr.s_addr = INADDR_ANY;
	}
	else {
		sock_addr.sin_addr.s_addr = inet_addr(hostname);
		if (sock_addr.sin_addr.s_addr == -1) {
			struct hostent *host_result = gethostbyname(hostname);
			if (host_result != NULL) {
				// Debug IP address
				struct in_addr addr = { 0, };
				addr.s_addr = *(u_long *)host_result->h_addr_list[0];
				printf("Resolved IP Address: %s\n", inet_ntoa(addr));

				sock_addr.sin_addr.s_addr = inet_addr(inet_ntoa(addr));
			}
		}
	}
	// ============================

	if (strcmp(protocol, "TCP") == 0) {
		socket_descriptor = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (socket_descriptor == INVALID_SOCKET) {
			printf("Socket failed. Error code: %i\n", WSAGetLastError());
			socket_cleanup();
			exit(1);
		}
	}
	else {
		socket_descriptor = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	}

	if (mode == SERVER_MODE) {
		iResult = bind(socket_descriptor, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));
		if (iResult == SOCKET_ERROR) {
			printf("Server: bind failed. Error code: %i\n", WSAGetLastError());
			socket_cleanup();
			exit(1);
		}

		display_thread = CreateThread(NULL, 0, displayThread, NULL, 0, NULL);
		printf("Server: create thread for statistics\n");

		if (strcmp(protocol, "TCP") == 0) {
			iResult = setsockopt(socket_descriptor, SOL_SOCKET, SO_SNDBUF, (char *)&bufsize, sizeof(bufsize));
			if (iResult < 0) {
				printf("Server: set socket buffer size error. Error code: %i\n", WSAGetLastError());
				socket_cleanup();
				exit(1);
			}
		}

		listen(socket_descriptor, 1);
		if (iResult == SOCKET_ERROR) {
			printf("Server: listen failed. Error code: %i\n", WSAGetLastError());
			socket_cleanup();
			exit(1);
		}

		if (strcmp(protocol, "TCP") == 0) {
			connector = accept(socket_descriptor, NULL, NULL);
			if (connector == INVALID_SOCKET) {
				printf("Server: accept failed. Error code: %i\n", WSAGetLastError());
				socket_cleanup();
				exit(1);
			}
			printf("Server: accepted incoming client\n");
		}
	}
	else {
		if (connect(socket_descriptor, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in)) == SOCKET_ERROR) {
			printf("Client: connect() failed. Error code: %i\n", WSAGetLastError());
			socket_cleanup();
			exit(1);
		}
		else {
			printf("Client: connected to server\n");
		}

		display_thread = CreateThread(NULL, 0, displayThread, NULL, 0, NULL);
		printf("Client: create thread for statistics\n");
	}
}

void sending_mode() {
	// printf("Sending Mode\n");
	// debug_args();

	socket_init();

	long sent_count = 0, iResult;
	char *sendbuf = (char *)calloc(pktsize, sizeof(char));
	ES_Timer timer = ES_Timer();

	while (sent_count < pktnum || pktnum == 0) {
		printf("Packet number: %d\n", sent_count);
		memset(sendbuf, 0, pktsize);
		sprintf(sendbuf, "%ld", sent_count);

		timer.Start();
		if (strcmp(protocol, "TCP") == 0) {
			iResult = send(socket_descriptor, sendbuf, pktsize, NULL);
		}
		else {
			iResult = sendto(socket_descriptor, sendbuf, pktsize, NULL, (struct sockaddr *) &sock_addr, sizeof(struct sockaddr_in));
		}
		double duration = timer.Elapsed();

		if (iResult == SOCKET_ERROR) {
			printf("Client: send failed. Error code: %i\n", WSAGetLastError());
			break;
		}
		else {
			printf("Client: send packet with size %d\n", iResult);
			accumulate_filesize[2] += (iResult);
			if (accumulate_filesize[2] > 1024) {
				accumulate_filesize[1] += (accumulate_filesize[2] / 1024);
				accumulate_filesize[2] = accumulate_filesize[2] % 1024;
			}
			if (accumulate_filesize[1] > 1024) {
				accumulate_filesize[0] += (accumulate_filesize[1] / 1024);
				accumulate_filesize[1] = accumulate_filesize[1] % 1024;
			}
		}

		if (pktrate > 0) {
			double sleep_time = (1000.0 / pktrate) * iResult - duration;
			if (sleep_time > 0) {
				Sleep(sleep_time);

				// double txrate = round(iResult * 1000.0 / (duration + sleep_time));
				// printf("Transfer rate after sleep: %.0f bytes/second\n", txrate);
			}
			else {
				// double txrate = round(iResult * 1000.0 / duration);
				// printf("Transfer rate: %.0f bytes/second\n", txrate);
			}
		}

		sent_count += 1;
	}

	free(sendbuf);
	socket_cleanup();
}

void receiving_mode() {
	// printf("Receiving Mode\n");
	// debug_args();

	socket_init();

	int iResult, buflen = pktsize;
	char *recvbuf;
	(char *)recvbuf = (char *)calloc(buflen, sizeof(char));
	// memset(recvbuf, 0, buflen);

	memset(&sender_addr, 0, sizeof(struct sockaddr_in));
	int sockaddr_len = sizeof(sender_addr);

	long expected_sequence = 0, incoming_time, previous_time = 0;
	do {
		if (strcmp(protocol, "TCP") == 0) {
			iResult = recv(connector, recvbuf, buflen, NULL);
		}
		else {
			iResult = recvfrom(socket_descriptor, recvbuf, buflen, NULL, (struct sockaddr *) &sender_addr, (socklen_t *) &sockaddr_len);
		}

		if (iResult == 0) {
			printf("Server: connection closing...\n");
			break;
		}
		else if (iResult == SOCKET_ERROR || iResult < 0) {
			printf("Server: recv failed. Error code: %i\n", WSAGetLastError());
			break;
		}
		else {
			incoming_time = program_timer.Elapsed();
			packet_received += 1;
			accumulate_filesize[2] += (iResult);
			if (accumulate_filesize[2] > 1024) {
				accumulate_filesize[1] += (accumulate_filesize[2] / 1024);
				accumulate_filesize[2] = accumulate_filesize[2] % 1024;
			}
			if (accumulate_filesize[1] > 1024) {
				accumulate_filesize[0] += (accumulate_filesize[1] / 1024);
				accumulate_filesize[1] = accumulate_filesize[1] % 1024;
			}

			double time_T = (double) (incoming_time - previous_time) / packet_received;
			// printf("Time T: %f, Incoming time: %d, Previous time: %d, Packet received: %d\n", time_T, incoming_time, previous_time, packet_received);
			jitter_new = (jitter_old * (packet_received - 1) + (incoming_time - previous_time - time_T)) / packet_received;

			jitter_old = jitter_new;
			previous_time = incoming_time;

			// printf("Server: receive packet with size %d\n", iResult);
			// printf("Server: sequence number %d of incoming packet\n", atoi(recvbuf));

			long sequence_number = atoi(recvbuf);
			if (sequence_number > expected_sequence) {
				packet_lost += (sequence_number - expected_sequence);
				expected_sequence = sequence_number + 1;
			}

			// printf("Server: receive from sender: (%s, %d)\n", inet_ntoa(sender_addr.sin_addr), ntohs(sender_addr.sin_port));
			
			expected_sequence += 1;
		}
	} while (iResult > 0);

	if (strcmp(protocol, "TCP") == 0) {
		iResult = shutdown(connector, SD_SEND);
		if (iResult == SOCKET_ERROR) {
			printf("Server: shutdown failed. Error code: %i\n", WSAGetLastError());
		}
	}

	free(recvbuf);
	socket_cleanup();
}

void host_info() {
	// printf("Host Information Mode\n");
	debug_args();

	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	printf("Hostname: %s\n", hostname);
	int i = 0;
	struct in_addr host_addr;
	struct hostent *host_result = gethostbyname(hostname);
	while (host_result->h_addr_list[i] != 0) {
		host_addr.s_addr = *(u_long *)host_result->h_addr_list[i];
		printf("IP Address %d: %s\n", i + 1, inet_ntoa(host_addr));
		i += 1;
	}

	WSACleanup();
}

int main(int argc, char **argv) {
	init_netprobe();

	args_parser(argc, argv);

	if (mode == SEND_MODE) {
		if (!hostname) {
			hostname = (char *)calloc(strlen("localhost") + 1, sizeof(char));
			strcpy(hostname, "localhost");
		}
		sending_mode();
	}
	else if (mode == RECV_MODE) {
		if (!hostname) {
			hostname = (char *)calloc(strlen("IN_ADDR_ANY") + 1, sizeof(char));
			strcpy(hostname, "IN_ADDR_ANY");
		}
		receiving_mode();
	}
	else if (mode == HOST_INFO) {
		if (!hostname) {
			hostname = (char *)calloc(strlen("localhost") + 1, sizeof(char));
			strcpy(hostname, "localhost");
		}
		host_info();
	}

	free_netprobe();
	return 0;
}